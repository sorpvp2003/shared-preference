import 'package:flutter/material.dart';
import 'package:sharedprefgone/home_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'main.dart';
class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyDashboard(),
    );
  }
}
class MyDashboard extends StatefulWidget {
  @override
  _MyDashboardState createState() => _MyDashboardState();
}
class _MyDashboardState extends State<MyDashboard> {
  late SharedPreferences logindata;
  late String email;
  @override
  void initState() {
    super.initState();
    initial();
  }
  void initial() async {
    logindata = await SharedPreferences.getInstance();
    setState(() {
      email = logindata.getString('email')!;
    });
  }
  @override
  Widget build(
      BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            "Login Page"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
              "https://upload.wikimedia.org/wikipedia/th/f/f7/Sugimoris025.png",
              height: 200,width:250,),
            SizedBox(height: 10,),
            Center(
              child: Text(
                'Welcome To Trainer',
                style: TextStyle(
                    fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(57, 20, 55, 65),
              child: Row(
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 90,vertical: 10,),
                      textStyle: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      logindata.setBool('login', true);
                      Navigator.pushReplacement(context,
                          new MaterialPageRoute(builder: (context) => MyApp()));
                    },
                    child: Text(''
                        'LogOut',style: TextStyle(
                        fontSize: 25,fontWeight: FontWeight.bold),),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}